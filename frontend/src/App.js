import React, { Component } from 'react'
import Decisions from './Decisions/Decisions';
import ReactDOM from 'react-dom';
import axios from 'axios'

export default class App extends Component {

  state = {
    questions: []
  }

  click = () => {

  
    var input = document.querySelector('#myFileInput');  
    
    const formData = new FormData();
    formData.append('image',input.files[0]);
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    };
    axios.post("/api/",formData,config)
        .then((response) => {
            alert("The file is successfully uploaded");
        }).catch((error) => {
    });


  }
  click2 = () =>{
    const config = {
      headers: {
          'content-type': 'multipart/form-data'
      }
    };
    var answers = document.querySelectorAll('.answers'), i;

    for (i = 0; i < answers.length; ++i) {
      const formDataAns = new FormData();

      formDataAns.append('answer',answers[i].value);

      axios.post("/api/answer/",formDataAns,config)
      .then((response) => {
          console.log("Succes")
          ReactDOM.render(<Decisions/>, document.getElementById('root'));
          }).catch((error) => {
      });
    }

  }



  componentDidMount() {
    axios.get("/api/questions/")
        .then((response) => {
          this.setState({questions: response.data});
        }).catch((error) => {
    });

  }

  render() {
    return (
      <div id="container">
        Your Picture: <input id="myFileInput" type="file" accept="image/*" capture="camera"></input>
        {
          this.state.questions.map(function(item, i){
            console.log({item});
            return <div><br></br>{item.question}<input class="answers" id={item.id} type="text"></input></div>
          })
        }  
        
        <br></br><br></br><button type="button" onClick={this.click}>PLAY1!</button>
        <br></br><br></br><button type="button" onClick={this.click2}>PLAY2!</button>
      </div>
    )
  }
}
