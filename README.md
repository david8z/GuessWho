# GuessWho

Web Game for HackUPC 2019.

---
![alt text](./title.png "")

---

Our project basically consisted on an interactive, easy and fast to use game. Were users will access to rooms with limited user access where they will be answering questions such as what is you're favorite color, what are you're most important threats... and then they will have to select wich user viewing only a selfy of the user, they think that the different statements that will be appearing in screen relates to. For example, who of the users of the room loves unicorns? This app tries to generate a good way of entretainment giving points to the users that has more success guessing when having short periods of time and most importantly it will try to eliminate prejudices of society showing that in most of the times people is not what they appear to be.

We decided to base our proyect on the web and eventhough it was thought to be a demo we wanted to focus on the backend and design more complex and scalable architeqtures trying to apply best practices. Unfortunately we weren't able to end up with a live DEMO for the end of the HackUPC, but we have enjoyed the experience and are happy with the amount of content and practice that we have obtained. Will continue developing if you are interested.

---

Technologies used:

- Backend (Django, Django Rest Framework).
- Backend WSGI Server (Gunicorn).
- Frontend (React.js).
- Load Balancer and Reverse Proxy (NGINX).
- Containerization (Docker).
- Orchestrator (Doccker Compose).
- DataBase (PostgreSQL).

![alt text](./architecture.png "")

We used NGINX as a reversed proxy that redirected the traffic either to our Backend, that was served by gunicorn giving us teh ability to deploy concurrently many workers that where composed of python based containers that run Django in union to Django Rest Framework. In the other hand NGINX in his own container served the static Files and static Media that was redirected thanks to the capabilities of Docker volumes and in last place the Frontend that was served by NGINX as it was composed of static compiled files, ready for production thanks to the power of React.

---
