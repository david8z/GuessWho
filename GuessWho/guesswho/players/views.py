from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from players.models import Player, PlayerAnswer
from players.serializers import PlayerSerializer, PlayerAnswerSerializer
from questions.models import Question

from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


class PlayerView(APIView):
    def get(self, request, format=None):
        player = Player.objects.all().prefetch_related('player_answer__answer')
        serializer = PlayerSerializer(player, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        serializer = PlayerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            if not self.request.session.exists(self.request.session.session_key):
                self.request.session.create()
            serializer.validated_data['session_id'] = self.request.session.session_key
            
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PlayerAnswerView(APIView):
    def post(self, request, format=None):
        serializer = PlayerAnswerSerializer(data=request.data)
    
def detail(request):
    try:
        isPlayer = Player.objects.filter(session_id=request.session.session_key)
    except Exception:
        print("")
    return render(request, 'index.html', {'isPlayer': isPlayer})