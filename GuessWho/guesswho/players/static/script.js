$("document").ready(function() {

    var $crf_token = $('[name="csrfmiddlewaretoken"]').attr('value');

    // INPUT
    $('#myFileInput').on("change", function() {

        $("#load_text").text("Uploading Picture...");

        var $files = $(this).get(0).files;

        if ($files.length) {

            // Reject big files
            if ($files[0].size > $(this).data("max-size") * 1024) {
                alert("Please select a smaller file");
                return false;
            }

            // Begin file upload
            console.log("Uploading image..");

            var form = new FormData();
            form.append("image", $files[0]);

            $.ajax({
                url: 'http://127.0.0.1:8090/api/',
                data: form,
                mimeType: 'multipart/form-data',
                method: "POST",
                crossDomain: true,
                processData: false,
                contentType: false,
                headers: {"X-CSRFToken": $crf_token},
                dataType: 'json',
                success: function(res) {
                    location.reload(true);
                },
                error: function() {
                    console.log("ERROR");
                }
            });


        }

    });

});

