from django.contrib import admin

from players.models import Player, PlayerAnswer
from questions.models import Question

admin.site.register(Player)
admin.site.register(Question)
admin.site.register(PlayerAnswer)
