from django.db import models
from questions.models import Question

class Player(models.Model):
    image = models.ImageField()
    points = models.PositiveIntegerField(default=0)
    name = models.TextField(null=True,)
    session_id = models.TextField(null=True, blank=True)

class PlayerAnswer(models.Model):
    answer = models.TextField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="player_answer", null=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="question_answer", null=True)

    def __str__(self):
        return self.answer