from rest_framework import serializers
from .models import Player, PlayerAnswer


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Player
        fields = ['image', 'session_id']

class PlayerAnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PlayerAnswer
        fields = ['answer']