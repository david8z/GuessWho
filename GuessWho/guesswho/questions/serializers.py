from rest_framework import serializers
from .models import Question


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    id = serializers.ReadOnlyField()
    class Meta:
        model = Question
        fields = ['id','question', 'answer_1', 'answer_2']
        