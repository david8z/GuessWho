from django.db import models

class Question(models.Model):
    question = models.TextField()
    answer_1 = models.TextField()
    answer_2 = models.TextField()

    def __str__(self):
        return self.question
