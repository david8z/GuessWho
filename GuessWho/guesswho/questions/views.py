from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from players.models import Question
from questions.serializers import QuestionSerializer

from django.shortcuts import render


class QuestionView(APIView):
    def get(self, request, format=None):
        question = Question.objects.all().prefetch_related("question_answer__answer")
        serializer = QuestionSerializer(question, many=True)
        return Response(serializer.data)