from django.contrib import admin
from django.urls import path

from players.views import PlayerView, PlayerAnswerView, detail
from questions.views import QuestionView

urlpatterns = [
    path('api/', PlayerView.as_view()),
    path('api/admin/', admin.site.urls),
    path('api/isPlayer/', detail, name='detail'),
    path('api/questions/', QuestionView.as_view()),
    path('api/answer/', PlayerAnswerView.as_view())
]
