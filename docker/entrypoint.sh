#!/bin/sh

cd GuessWho/guesswho

# collect static files
python manage.py collectstatic --noinput

python manage.py makemigrations

python manage.py migrate --noinput

exec "$@"